# GAUDI manager server

Api doc can be found [here](https://documenter.getpostman.com/view/7788033/TzJvdbxj)

# Requirements:

## 1. Envs

Create ".env" file in project's root with the following evn variables:

- PORT=3000 (by default)
- MODE=(development/production)
- GAUDI_ORIGIN=<https://<origin>>
- GAUDI_MOCK= (mocked server for development, I've used [Postman](https://learning.postman.com/docs/designing-and-developing-your-api/mocking-data/setting-up-mock/))
- MINIO_ENDPOINT=127.0.0.1:9000 (by default)
- MINIO_ACCESS_KEY=your_access_key
- MINIO_SECRET_KEY=your_secret_key
- MINIO_BUCKET_NAME=jobs (by default)

## 2. Configs

### minio.config.ts

Go to src/config/minio.config.ts and set up next varibales:

```js
{
  endPoint: < Minio service name in docker-compose.yml ('minio' by default) >,
  port: < Minio service port (9000 by default)>,
  useSSL: false,
  accessKey: process.env.MINIO_ACCESS_KEY,
  secretKey: process.env.MINIO_SECRET_KEY,
}
```

## 3. Dockerfile

Uncomment either PRODUCTION or DEVELOPMENT version of build

## 4. Docker-compose

Uncomment one of these :

```js
    # for development:
    command: /bin/sh -c "npm run start:dev"
```

```js
    # for production:
    command: /bin/sh -c "npm run prod"
```

## 5. Run

```sh
docker-compose up --build
```
