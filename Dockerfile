###  PRODUCTION  ###
# Stage 1
FROM node:lts-alpine@sha256:b2da3316acdc2bec442190a1fe10dc094e7ba4121d029cb32075ff59bb27390a as build
WORKDIR /usr/src/gaudi-manager
COPY package*.json /usr/src/gaudi-manager/
COPY tsconfig.json /usr/src/gaudi-manager/
COPY src /usr/src/gaudi-manager/src
RUN npm install && npm install typescript -g
RUN npm run build
COPY . .
# Stage 2
FROM node:lts-alpine@sha256:b2da3316acdc2bec442190a1fe10dc094e7ba4121d029cb32075ff59bb27390a
EXPOSE 3000
ENV NODE_ENV production
WORKDIR /usr/src/gaudi-manager-prod
COPY --from=build /usr/src/gaudi-manager/dist /usr/src/gaudi-manager-prod/dist
COPY --from=build /usr/src/gaudi-manager/.env /usr/src/gaudi-manager-prod/
COPY --from=build /usr/src/gaudi-manager/package*.json /usr/src/gaudi-manager-prod/
RUN npm ci --only=production


###  DEVELOPMENT  ###

# FROM node:lts-alpine@sha256:b2da3316acdc2bec442190a1fe10dc094e7ba4121d029cb32075ff59bb27390a
# ENV NODE_ENV development
# WORKDIR /usr/src/gaudi-manager
# COPY package*.json /usr/src/gaudi-manager/
# RUN npm install
# COPY . .

