export type Nullable<T> = T | null;
export type Possibly<T> = T | undefined;
