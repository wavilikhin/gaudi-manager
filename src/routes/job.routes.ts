import { Application } from "express";
import * as mongoose from "mongoose";
import { JobSchema } from "../models/job.model";
import { JobController } from "../controllers/job/job.controller";
import { QueueController } from "../controllers/queue/queue.controller";
import { upload as fileUploader } from "../config/multer.config";
import { TourServerController } from "../controllers/tourServer/tourServer.controller";
import { ReconstructionServerController } from "../controllers/reconstructionServer/reconstructionServer.controller";
import { gaudiApi } from "../api/gaudi";

const Job = mongoose.model("Job", JobSchema);

const tourServerController = new TourServerController(gaudiApi);
const reconstructionServerController = new ReconstructionServerController(
    gaudiApi
);

const jobController = new JobController();
const queueController = new QueueController(
    Job,
    tourServerController,
    reconstructionServerController
);

export class Routes {
    public routes(app: Application): void {
        app.route("/jobs/update").get(jobController.updateAllJobs);

        app.route("/jobs")
            .get(jobController.getAllJobs)
            .post(jobController.createNewJob);

        app.route("/jobs/:id")
            .get(jobController.getJob)
            .delete(jobController.deleteJob);

        app.route("/jobs/:id/status/tour").get(jobController.updateTourStatus);

        app.route("/jobs/:id/status/reconstruction").get(
            jobController.updateReconstructionStatus
        );

        app.route("/jobs/:id/build/tour").post(jobController.startTourBuild);

        app.route("/jobs/:id/build/reconstruction").post(
            jobController.startReconstructionBuild
        );

        app.route("/jobs/:id/files/").post(
            fileUploader.single("file"),
            jobController.uploadFile
        );

        app.route("/queue/reconstruction/").post(
            queueController.updateReconstructionStatus
        );

        app.route("/queue/tour/").post(queueController.updateTourStatus);
    }
}
