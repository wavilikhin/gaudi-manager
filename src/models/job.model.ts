import * as mongoose from "mongoose";

const Schema = mongoose.Schema;

export const JobSchema = new Schema({
    id: {
        type: String,
        required: true,
    },
    created: {
        type: Date,
        required: true,
    },
    source: {
        images: [String],
        video: String,
    },
    status: {
        tour: String,
        reconstruction: String,
    },
    files: {
        model: String,
        json: String,
    },
});
