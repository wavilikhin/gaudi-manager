import * as mongoose from "mongoose";

const Schema = mongoose.Schema;

export const QueueSchema = new Schema({
    id: {
        type: String,
        required: true,
    },
    created: {
        type: Date,
        required: true,
    },
    tasks: [String],
});
