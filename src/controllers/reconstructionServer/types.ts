export interface IReconstructionServerController {
    updateCurrentJob(id: string): void;
    isBuisy(): Promise<boolean>;
    startJobReconstructionBuild(id: string): Promise<string>;
}
