import { AxiosInstance } from "axios";
import { Error } from "mongoose";
import { Nullable } from "types.global";
import { devLog } from "../../utils/logger";
import { IReconstructionServerController } from "./types";

export class ReconstructionServerController
    implements IReconstructionServerController {
    private static currentJob = "";

    private static _instance: Nullable<ReconstructionServerController> = null;

    protected static api: AxiosInstance;

    constructor(reconstructionServerApi: AxiosInstance) {
        if (ReconstructionServerController._instance) {
            return ReconstructionServerController._instance;
        }

        ReconstructionServerController._instance = this;
        ReconstructionServerController.api = reconstructionServerApi;
    }

    updateCurrentJob(id: string): void {
        ReconstructionServerController.currentJob = id;
    }

    async isBuisy(): Promise<boolean> {
        devLog("Trying to get reconstruction-service current status");
        const serverStatusRequest = await ReconstructionServerController.api.get(
            "/tour/status"
        );
        const status: { id: string } = serverStatusRequest.data;
        devLog(`Status: ${status}`);
        devLog("-- Success");

        if (status.id) {
            devLog("Updating current job id");
            ReconstructionServerController.currentJob = status.id;
        }
        devLog(`Current job id = ${status.id}`);
        devLog("-- Success");

        return ReconstructionServerController.currentJob !== "";
    }

    async startJobReconstructionBuild(id: string): Promise<string> {
        if (ReconstructionServerController.currentJob) {
            throw new Error("Tour server is currently buisy");
        }

        try {
            const tourBuildReq = await ReconstructionServerController.api.post(
                `/reconstruction/jobs/${id}/build`
            );

            if (tourBuildReq.status === 200) {
                return id;
            }

            throw new Error(
                `Tour build request finished with status ${tourBuildReq.status}`
            );
        } catch (error) {
            throw new Error(error);
        }
    }
}
