import { AxiosInstance } from "axios";
import { Error } from "mongoose";
import { Nullable } from "types.global";
import { devLog } from "../../utils/logger";
import { ITourServerController } from "./types";

export class TourServerController implements ITourServerController {
    private static currentJob = "";

    private static _instance: Nullable<TourServerController> = null;

    protected static api: AxiosInstance;

    constructor(tourServerApi: AxiosInstance) {
        if (TourServerController._instance) {
            return TourServerController._instance;
        }

        TourServerController._instance = this;
        TourServerController.api = tourServerApi;
    }

    updateCurrentJob(id: string): void {
        TourServerController.currentJob = id;
    }

    async isBuisy(): Promise<boolean> {
        devLog("Trying to get 360-tour-service current status");

        const serverStatusRequest = await TourServerController.api.get(
            "/tour/status"
        );

        const status: { id: string } = serverStatusRequest.data;
        devLog(`Status: ${status}`);
        devLog("-- Success");

        if (status.id) {
            devLog("Updating current job id");
            TourServerController.currentJob = status.id;
        }
        devLog(`Current job id = ${status.id}`);
        devLog("-- Success");

        return TourServerController.currentJob !== "";
    }

    async startJobTourBuild(id: string): Promise<string> {
        if (TourServerController.currentJob) {
            throw new Error("Tour server is currently buisy");
        }

        try {
            const tourBuildReq = await TourServerController.api.post(
                `/tour/jobs/${id}/build`
            );

            if (tourBuildReq.status === 200) {
                return id;
            }

            throw new Error(
                `Tour build request finished with status ${tourBuildReq.status}`
            );
        } catch (error) {
            throw new Error(error);
        }
    }
}
