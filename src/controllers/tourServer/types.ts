export interface ITourServerController {
    updateCurrentJob(id: string): void;
    isBuisy(): Promise<boolean>;
    startJobTourBuild(id: string): Promise<string>;
}
