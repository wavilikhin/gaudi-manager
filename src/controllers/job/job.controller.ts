import * as fs from "fs";
import { Request, Response } from "express";
import * as mongoose from "mongoose";
import { JobSchema } from "../../models/job.model";

import { v4 as uuid } from "uuid";
import { gaudiApi } from "../../api/gaudi";
import { minioClient } from "../../config/minio.config";
import { Job as JobType, JobStatusResponse } from "./types";
import { getJobStatus } from "../../utils/jobs";
import { TourServerController } from "../../controllers/tourServer/tourServer.controller";
import { ReconstructionServerController } from "../../controllers/reconstructionServer/reconstructionServer.controller";

import { QueueController } from "../queue/queue.controller";
import { devLog } from "../../utils/logger";

const Job = mongoose.model("Job", JobSchema);

const tourServerController = new TourServerController(gaudiApi);
const reconstructionServerController = new ReconstructionServerController(
    gaudiApi
);

const queueController = new QueueController(
    Job,
    tourServerController,
    reconstructionServerController
);

export class JobController {
    // Update all jobs statuses
    public async updateAllJobs(
        _req: Request,
        res: Response
    ): Promise<Response> {
        try {
            const jobsToUpdate: { id: string; update: string[] }[] = [];
            const jobs: JobType[] = await Job.find({}).lean();

            // Get list of jobs wich possibly needs update
            for (const job of jobs) {
                const statusesToUpdate = [];

                if (job.status.tour !== "done") {
                    statusesToUpdate.push("tour");
                }
                if (job.status.reconstruction !== "done") {
                    statusesToUpdate.push("reconstruction");
                }
                if (statusesToUpdate.length > 0) {
                    jobsToUpdate.push({ id: job.id, update: statusesToUpdate });
                }
            }

            const updateRequests: Promise<{
                id: string;
                status: { tour?: string; reconstruction?: string };
            }>[] = [];

            for (const job of jobsToUpdate) {
                updateRequests.push(getJobStatus(job));
            }

            const updatedJobs = await Promise.all(updateRequests);

            for await (const job of updatedJobs) {
                if (job.status.tour) {
                    await Job.findOneAndUpdate(
                        {
                            id: job.id,
                        },
                        {
                            $set: { "status.tour": job.status.tour },
                        }
                    );
                }

                if (job.status.reconstruction) {
                    await Job.findOneAndUpdate(
                        {
                            id: job.id,
                        },
                        {
                            $set: {
                                "status.reconstruction":
                                    job.status.reconstruction,
                            },
                        }
                    );
                }
            }

            return res.sendStatus(200);
        } catch (error) {
            if (process.env.NODE_ENV === "development") {
                console.error(error);
            }
            return res.sendStatus(500);
        }
    }

    // Fetch all avaliable jobs
    public async getAllJobs(_req: Request, res: Response): Promise<Response> {
        try {
            const jobs = await Job.find({});
            return res.status(200).json(jobs);
        } catch (error) {
            if (process.env.NODE_ENV === "development") {
                console.error(error);
            }
            return res.sendStatus(500);
        }
    }

    // Fetch job by id
    public async getJob(req: Request, res: Response): Promise<Response> {
        try {
            const { id } = req.params;

            const job = await Job.findOne({ id });

            if (!job) {
                return res.sendStatus(404);
            }

            return res.status(200).json(job);
        } catch (error) {
            if (process.env.NODE_ENV === "development") {
                console.error(error);
            }
            return res.sendStatus(500);
        }
    }

    // Create new job
    public async createNewJob(_req: Request, res: Response): Promise<Response> {
        try {
            const jobId = uuid();

            const job = new Job({
                id: jobId,
                created: new Date(Date.now()),
                source: {
                    images: [],
                    video: "",
                },
                status: {
                    tour: "",
                    reconstruction: "",
                },
                files: {
                    model: "",
                    json: "",
                },
            });

            await job.save();

            return res.status(201).json(job);
        } catch (error) {
            if (process.env.NODE_ENV === "development") {
                console.error(error);
                return res.status(500).json({ error });
            }
            return res.sendStatus(500);
        }
    }

    // Delete job by id
    public async deleteJob(req: Request, res: Response): Promise<Response> {
        try {
            const { id } = req.params;

            const job = await Job.findOneAndDelete({ id });

            if (!job) {
                return res.sendStatus(404);
            }

            return res.status(200).json(job);
        } catch (error) {
            if (process.env.NODE_ENV === "development") {
                console.error(error);
                return res.status(500).json({ error });
            }
            return res.sendStatus(500);
        }
    }

    // Get tour status
    public async updateTourStatus(
        req: Request,
        res: Response
    ): Promise<Response> {
        try {
            const { id } = req.params;

            const job = await Job.findOne({ id });

            if (!job) {
                return res.sendStatus(404);
            }

            const statusRequest = await gaudiApi.get(`/tour/jobs/${id}/`);
            const statusResponse: JobStatusResponse = statusRequest.data;

            if (statusRequest.status === 200) {
                Job.findOneAndUpdate(
                    { id },
                    { status: { tour: statusResponse.data.attributes.status } }
                );
            }

            return res.sendStatus(204);
        } catch (error) {
            if (process.env.NODE_ENV === "development") {
                console.error(error);
                return res.status(500).json({ error });
            }
            return res.sendStatus(500);
        }
    }

    // Get reconstruction status
    public async updateReconstructionStatus(
        req: Request,
        res: Response
    ): Promise<Response> {
        try {
            const { id } = req.params;

            const job = await Job.findOne({ id });

            if (!job) {
                return res.sendStatus(404);
            }

            const statusRequest = await gaudiApi.get(
                `/reconstruction/jobs/${id}/`
            );
            const statusResponse: JobStatusResponse = statusRequest.data;

            if (statusRequest.status === 200) {
                Job.findOneAndUpdate(
                    { id },
                    {
                        status: {
                            reconstruction:
                                statusResponse.data.attributes.status,
                        },
                    }
                );
            }

            return res.sendStatus(204);
        } catch (error) {
            if (process.env.NODE_ENV === "development") {
                console.error(error);
                return res.status(500).json({ error });
            }
            return res.sendStatus(500);
        }
    }

    // Start tour build
    public async startTourBuild(
        req: Request,
        res: Response
    ): Promise<Response> {
        try {
            const { id } = req.params;
            devLog(`Start tour request for job ${id}`);

            const job = await Job.findOne({ id });

            if (!job) {
                devLog(`Job with id ${id} doesnt exist`);
                return res.sendStatus(404);
            }

            devLog(`Trying to start tour build for job ${job.id}`);

            // Добавляем джобу в очередь
            await queueController.push(id, ["tour"]);
            devLog(`Job ${job.id} is added to queue`);

            // Обновляем статусы по туру и реконструкции
            await job.updateOne({
                status: { reconstruction: "waiting", tour: "waiting" },
            });
            devLog(`Job's ${job.id} statuses updated`);

            return res.sendStatus(204);
        } catch (error) {
            if (process.env.NODE_ENV === "development") {
                console.error(error);
                return res.status(500).json({ error });
            }
            return res.sendStatus(500);
        }
    }

    // Start reconstruction build
    public async startReconstructionBuild(
        req: Request,
        res: Response
    ): Promise<Response> {
        try {
            const { id } = req.params;

            const job = await Job.findOne({ id });

            if (!job) {
                return res.sendStatus(404);
            }

            const buildReq = await gaudiApi.post(
                `/reconstruction/jobs/${id}/build`
            );

            if (buildReq.status === 200) {
                Job.findOneAndUpdate(
                    { id },
                    { status: { reconstruction: "building" } }
                );
            }

            return res.sendStatus(204);
        } catch (error) {
            if (process.env.NODE_ENV === "development") {
                console.error(error);
                return res.status(500).json({ error });
            }
            return res.sendStatus(500);
        }
    }

    // Upload file
    public async uploadFile(req: Request, res: Response): Promise<Response> {
        if (req.file) {
            const { id } = req.params;

            const fileName = req.file.originalname.trim();
            const filePath = req.file.path.trim();
            const fileStream = fs.createReadStream(filePath);
            const size = req.file.size;
            const mimeType = req.file.mimetype.split("/")[0];

            let storagePath;
            if (mimeType == "video") {
                storagePath = `${id}/${fileName}`;
            } else {
                storagePath = `${id}/images/${fileName}`;
            }

            const bucketExists = await minioClient.bucketExists(
                process.env.MINIO_BUCKET_NAME
            );

            if (!bucketExists) {
                try {
                    await minioClient.makeBucket(process.env.MINIO_BUCKET_NAME);
                } catch (error) {
                    console.error("Minio.makeBucket failed:\n", error);
                    if (process.env.NODE_ENV === "development") {
                        return res.status(500).json({ error: error });
                    }
                    return res.sendStatus(500);
                }
            }

            try {
                await minioClient.putObject(
                    "jobs",
                    storagePath,
                    fileStream,
                    size
                );

                fs.rmdirSync(req.file.destination, { recursive: true });

                switch (mimeType) {
                    case "image":
                        await Job.findOneAndUpdate(
                            { id },
                            { $push: { "source.images": fileName } }
                        );
                        break;
                    case "video":
                        await Job.findOneAndUpdate(
                            { id },
                            { $set: { "source.video": fileName } }
                        );
                        break;
                    default:
                        break;
                }

                return res.sendStatus(201);
            } catch (error) {
                console.error("Minio.putObject failed:\n", error);
                if (process.env.NODE_ENV === "development") {
                    return res.status(500).json({ error: error });
                }
                return res.sendStatus(500);
            }
        }
        devLog(`No file provided for upload`);
        return res.sendStatus(500);
    }
}
