export type Statuses = "empty" | "done" | "building" | "error";

export type JobStatusResponse = {
    data: {
        id: string;
        type: "tour" | "reconstruction";
        attributes: {
            status: Statuses;
        };
    };
};

export type Job = {
    id: string;
    created: string;
    status: {
        tour: Statuses;
        reconstruction: Statuses;
    };
    files: {
        model: string[];
        json: string[];
    };
};
