import * as mongoose from "mongoose";
import { Request, Response } from "express";
import {
    IQueueController,
    QueueItem,
    QueueItem as TQueueItem,
    IQueueItem,
    TourStatus,
} from "./types";
import { QueueSchema } from "../../models/queue.model";
import { Nullable, Possibly } from "../../types.global";
import { ITourServerController } from "controllers/tourServer/types";
import { IReconstructionServerController } from "controllers/reconstructionServer/types";
import { devLog } from "../../utils/logger";

const QueueItem = mongoose.model<IQueueItem>("Queue", QueueSchema);

export class QueueController implements IQueueController {
    private static _instance: Nullable<QueueController> = null;
    private static jobs: mongoose.Model<
        mongoose.Document<unknown, Record<string, unknown>>,
        Record<string, unknown>
    >;
    private static tourServer: ITourServerController;
    private static reconstructionServer: IReconstructionServerController;
    length = 0;

    constructor(
        JobsController: mongoose.Model<
            mongoose.Document<unknown, Record<string, unknown>>,
            Record<string, unknown>
        >,
        TourServerController: ITourServerController,
        ReconstructionServerController: IReconstructionServerController
    ) {
        if (QueueController._instance) {
            return QueueController._instance;
        }

        QueueController.tourServer = TourServerController;
        QueueController.reconstructionServer = ReconstructionServerController;
        QueueController.jobs = JobsController;
        QueueController._instance = this;

        QueueController.queueStatusLoop();
    }
    // Add to queue
    async push(id: string, tasks: string[]): Promise<void> {
        try {
            const queueItem = {
                id,
                created: new Date(Date.now()),
                tasks,
            };

            await new QueueItem(queueItem).save();

            switch (tasks[0]) {
                case "tour":
                    if (
                        !QueueController.tourServer?.isBuisy() &&
                        this.length === 0
                    ) {
                        try {
                            const jobId = await QueueController.tourServer?.startJobTourBuild(
                                id
                            );

                            await QueueController.jobs?.findOneAndUpdate(
                                { id: jobId },
                                {
                                    $set: { "status.tour": "building" },
                                }
                            );
                        } catch (error) {
                            devLog(
                                `Error while starting tour build for job ${id}}, ${error}`
                            );
                        }
                    }
                    break;
                case "reconstruction":
                    if (
                        !QueueController.reconstructionServer?.isBuisy() &&
                        this.length === 0
                    ) {
                        try {
                            const jobId = await QueueController.reconstructionServer?.startJobReconstructionBuild(
                                id
                            );

                            await QueueController.jobs?.findOneAndUpdate(
                                { id: jobId },
                                {
                                    $set: {
                                        "status.reconstruction": "building",
                                    },
                                }
                            );
                        } catch (error) {
                            devLog(
                                `Error while starting reconstruction build for job ${id}}, ${error}`
                            );
                        }
                    }
                    break;
                default:
                    devLog(`Unknown task ${tasks[0]}`);
                    break;
            }

            this.length++;
        } catch (error) {
            console.error(`QueueController .push() failed: \n`, error);
        }
    }

    // Remove from queue
    async pop(): Promise<Possibly<TQueueItem>> {
        try {
            const nextQueueItem: TQueueItem[] = await QueueItem.find({})
                .sort({ created: 1 })
                .limit(1)
                .lean();

            if (nextQueueItem[0]) {
                await QueueItem.findOneAndDelete({ id: nextQueueItem[0].id });
            }

            this.length--;
            return nextQueueItem[0];
        } catch (error) {
            console.error(`QueueController .pop() failed: \n`, error);
        }
    }

    // Update job's required tasks
    async update(id: string, tasks: string[]): Promise<void> {
        try {
            let queueItemTasks: Possibly<string[]> = await QueueItem.findOne(
                {
                    id,
                },
                "tasks"
            ).lean();

            if (!queueItemTasks) {
                throw new Error(
                    `QueueController .update() error: job with id ${id} does not exist`
                );
            }

            for (const task of tasks) {
                if (queueItemTasks.some((t) => t === task)) {
                    queueItemTasks = queueItemTasks.filter((t) => t !== task);
                } else {
                    queueItemTasks.push(task);
                }
            }

            await QueueItem.findOneAndUpdate({ id }, { tasks: queueItemTasks });
        } catch (error) {
            console.error(`QueueController .update() failed: \n`, error);
        }
    }

    // Handler for request from TourGeneratorServer
    public async updateTourStatus(
        req: Request,
        res: Response
    ): Promise<Response> {
        try {
            const data: TourStatus = req.body;

            const { id, status } = data;
            devLog(`New update tour request: id: ${id}, status: ${status}`);

            if (id && status) {
                // 1. Убираем 'tour' из тасок джобы в очереди
                devLog(`Trying to update job ${id} in queue collection`);
                await QueueItem.findOneAndUpdate(
                    { id },
                    { $pull: { tasks: "tour" } }
                ).catch((error) => {
                    devLog(
                        `Error occcured while updating queue item: ${error.stack}`
                    );
                    throw new Error(error.stack);
                });
                devLog(`-- Success`);

                // 2. Обновляем статус в Монге
                devLog(`Trying to update job ${id} in jobs collection`);
                await QueueController.jobs
                    ?.findOneAndUpdate(
                        { id },
                        { $set: { "status.tour": status } }
                    )
                    .catch((error) => {
                        devLog(
                            `Error occcured while updating job status in Mongo: ${error.stack}`
                        );
                        throw new Error(error.stack);
                    });
                devLog(`-- Success`);

                // 3. Добавляем в таски 'reconstruction'
                devLog(
                    `Trying to add 'reconstruction' to job ${id} in queue collection`
                );
                try {
                    const job = await QueueItem.findOne({
                        id,
                    });

                    if (
                        job &&
                        !job.tasks.some(
                            (taskname: string) => taskname === "reconstruction"
                        )
                    ) {
                        job.tasks.push("reconstruction");
                        await job.save();
                    }
                } catch (error) {
                    devLog(
                        `Error occcured while adding new task to queue item: ${error.stack}`
                    );
                    throw new Error(error.stack);
                }
                devLog(`-- Success`);

                // 4. Ищем следующую джобу с таской 'tour'
                devLog(
                    `Trying to find next job with 'tour' task in queue collection`
                );
                const nextQueueItem: TQueueItem[] = await QueueItem.find({
                    tasks: "tour",
                })
                    .sort({ created: 1 })
                    .limit(1)
                    .lean();
                devLog(`-- Success`);

                devLog(`Next queue item: <${nextQueueItem}>,`);

                // 5. Отправляем ее на билд
                if (nextQueueItem.length > 0) {
                    try {
                        devLog(
                            `Trying to start 'tour' build for next job ${nextQueueItem[0].id}`
                        );
                        await QueueController.tourServer
                            .startJobTourBuild(nextQueueItem[0].id)
                            .catch((error) => {
                                devLog(
                                    `Error occcured while starting job's tour build: ${error.stack}`
                                );
                                throw new Error(error.stack);
                            });
                        devLog(`-- Success`);

                        // 5.1 Обновляем статус в контроллере (id новой джобы)
                        devLog(`Updating tour server status`);
                        QueueController.tourServer?.updateCurrentJob(
                            nextQueueItem[0].id
                        );
                        devLog(`-- Success`);

                        // 5.3 Обновляем статус в Монге
                        devLog(
                            `Trying to update job ${nextQueueItem[0].id} in jobs collection`
                        );
                        await QueueController.jobs
                            ?.findOneAndUpdate(
                                { id: nextQueueItem[0].id },
                                { $set: { "status.tour": "building" } }
                            )
                            .catch((error) => {
                                devLog(
                                    `Error occcured while updating job's status in Mongo: ${error.stack}`
                                );
                                throw new Error(error.stack);
                            });
                        devLog(`-- Success`);
                    } catch (error) {
                        devLog(
                            `Error while starting tour build for job ${nextQueueItem[0].id}}, ${error}`
                        );
                        return res.sendStatus(500);
                    }
                }
                // 6. Если в очереди нет задач на 'tour' обновляем статус в контроллере (сервер освободился)
                devLog(`Updating tour server status`);
                QueueController.tourServer?.updateCurrentJob("");
                devLog(`-- Success`);

                devLog(`No more items with 'tour' tasks in queue`);
                return res.sendStatus(200);
            }
            devLog(`Update tour status: No id or status provided :${data}`);
            return res.sendStatus(500);
        } catch (error) {
            devLog(
                `Error occured while updating job's tour status ${error.stack}`
            );
            return res.sendStatus(500);
        }
    }

    // Handler for request from ReconstructionServer
    public async updateReconstructionStatus(
        req: Request,
        res: Response
    ): Promise<Response> {
        try {
            const data: TourStatus = req.body;

            const { id, status } = data;
            devLog(
                `New update reconstruction request: id: ${id}, status: ${status}`
            );

            if (id && status) {
                // 1. Убираем таску на reconstruction из очереди
                devLog(`Trying to update job ${id} in queue collection`);
                await QueueItem.findOneAndUpdate(
                    { id },
                    { $pull: { tasks: "reconstruction" } }
                ).catch((error) => {
                    devLog(
                        `Error occcured while updating queue item: ${error.stack}`
                    );
                    throw new Error(error.stack);
                });
                devLog(`-- Success`);

                // 2. Обновляем статус в Монге
                devLog(`Trying to update job ${id} in jobs collection`);
                await QueueController.jobs
                    ?.findOneAndUpdate(
                        { id },
                        { $set: { "status.reconstruction": status } }
                    )
                    .catch((error) => {
                        devLog(
                            `Error occcured while updating job status in Mongo: ${error.stack}`
                        );
                        throw new Error(error.stack);
                    });
                devLog(`-- Success`);

                // 2.1 Обновляем статус в контроллере (сервер освободился)
                devLog(`Updating tour server status`);
                QueueController.reconstructionServer?.updateCurrentJob("");
                devLog(`-- Success`);

                // 3.Смотрим есть ли еще таски
                devLog(
                    `Trying to check other tasks for job ${id} in queue collection`
                );
                try {
                    const job = await QueueItem.findOne({ id });

                    // 3.1 Если тасок больше нет - удаляем
                    if (job && job.tasks.length === 0) {
                        await QueueItem.findOneAndDelete({ id });
                    }
                } catch (error) {
                    devLog(
                        `Error occcured deleting job ${id} from queue collection: ${error.stack}`
                    );
                    throw new Error(error.stack);
                }
                devLog(`-- Success`);

                // 4. Ищем следующую джобу с таской reconstruction
                devLog(
                    `Trying to find next job with 'reconstruction' task in queue collection`
                );
                const nextQueueItem: TQueueItem[] = await QueueItem.find({
                    tasks: "reconstruction",
                })
                    .sort({ created: 1 })
                    .limit(1)
                    .lean();

                devLog(`-- Success`);

                devLog(`Next queue item: <${nextQueueItem}>,`);

                // 5. Отправляем ее на билд
                if (nextQueueItem.length > 0) {
                    try {
                        devLog(
                            `Trying to start 'reconstruction' build for next job ${nextQueueItem[0].id}`
                        );

                        await QueueController.reconstructionServer
                            .startJobReconstructionBuild(nextQueueItem[0].id)
                            .catch((error) => {
                                devLog(
                                    `Error occcured while starting job's reconstruction build: ${error.stack}`
                                );
                                throw new Error(error.stack);
                            });
                        devLog(`-- Success`);

                        // 5.2 Обновляем статус в Монге
                        devLog(
                            `Trying to update job ${nextQueueItem[0].id} in jobs collection`
                        );
                        await QueueController.jobs
                            ?.findOneAndUpdate(
                                { id: nextQueueItem[0].id },
                                {
                                    $set: {
                                        "status.reconstruction": "building",
                                    },
                                }
                            )
                            .catch((error) => {
                                devLog(
                                    `Error occcured while updating job's status in Mongo: ${error.stack}`
                                );
                                throw new Error(error.stack);
                            });
                        devLog(`-- Success`);
                    } catch (error) {
                        devLog(
                            `Error while starting reconstruction build for job ${nextQueueItem[0].id}}, ${error}`
                        );
                        return res.sendStatus(500);
                    }
                }
                devLog(`No more items with 'reconstruction' tasks in queue`);
                return res.sendStatus(200);
            }
            devLog(
                `Update reconstruction status: No id or status provided :${data}`
            );
            return res.sendStatus(500);
        } catch (error) {
            devLog(
                `Error occured while updating job's reconstruction status ${error.stack}`
            );
            return res.sendStatus(500);
        }
    }

    static async queueStatusLoop(): Promise<void> {
        devLog("Queue status loop started");
        const queueLength = await QueueItem.countDocuments();

        if (queueLength > 0) {
            const nextQueueItem = await QueueItem.find({})
                .sort({ created: 1 })
                .limit(1);

            if (nextQueueItem[0]) {
                try {
                    const nextTask = nextQueueItem[0].tasks.shift();

                    switch (nextTask) {
                        case "tour":
                            try {
                                // Смотрим статус сервера
                                devLog(`Trying to get server status`);
                                const serverIsBuisy = await QueueController.tourServer.isBuisy();
                                devLog(
                                    `360-tour-service is busy : ${serverIsBuisy}`
                                );

                                // Если занят - выходим
                                if (serverIsBuisy) {
                                    break;
                                }

                                // Если нет - запускаем джобу
                                devLog(
                                    `Trying to start 'tour' build for next job ${nextQueueItem[0].id}`
                                );
                                // Отправляем запрос
                                await QueueController.tourServer
                                    .startJobTourBuild(nextQueueItem[0].id)
                                    .catch((error) => {
                                        devLog(
                                            `Error occcured while starting job's tour build: ${error.stack}`
                                        );
                                        throw new Error(error.stack);
                                    });
                                devLog(`-- Success`);

                                // Обновляем статус в Монге
                                devLog(
                                    `Trying to update job ${nextQueueItem[0].id} in jobs collection`
                                );
                                await QueueController.jobs
                                    .findOneAndUpdate(
                                        { id: nextQueueItem[0].id },
                                        { $set: { "status.tour": "building" } }
                                    )
                                    .catch((error) => {
                                        devLog(
                                            `Error occcured while updating job's status in Mongo: ${error.stack}`
                                        );
                                        throw new Error(error.stack);
                                    });
                                devLog(`-- Success`);

                                break;
                            } catch (error) {
                                devLog(
                                    `Error occcured in queue loop: ${error.stack}`
                                );
                                break;
                            }

                        case "reconstruction":
                            // Смотрим статус сервера
                            const recServerIsBuisy = await QueueController.reconstructionServer.isBuisy();

                            // Если занят - выходим
                            if (recServerIsBuisy) {
                                break;
                            }

                            // Если нет - запускаем джобу
                            devLog(
                                `Trying to start 'reconstruction' build for next job ${nextQueueItem[0].id}`
                            );

                            // Отправляем запрос
                            await QueueController.reconstructionServer
                                .startJobReconstructionBuild(
                                    nextQueueItem[0].id
                                )
                                .catch((error) => {
                                    devLog(
                                        `Error occcured while starting job's reconstruction build: ${error.stack}`
                                    );
                                    throw new Error(error.stack);
                                });
                            devLog(`-- Success`);

                            // Обновляем статус в Монге
                            devLog(
                                `Trying to update job ${nextQueueItem[0].id} in jobs collection`
                            );

                            await QueueController.jobs
                                ?.findOneAndUpdate(
                                    { id: nextQueueItem[0].id },
                                    {
                                        $set: {
                                            "status.reconstruction": "building",
                                        },
                                    }
                                )
                                .catch((error) => {
                                    devLog(
                                        `Error occcured while updating job's status in Mongo: ${error.stack}`
                                    );
                                    throw new Error(error.stack);
                                });
                            devLog(`-- Success`);
                            break;
                    }
                } catch (error) {
                    devLog(`Error in queue status loop:  ${error.stack}`);
                }
            }
        }

        setTimeout(QueueController.queueStatusLoop, 3000);
        devLog("Queue status loop finished");
    }
}
