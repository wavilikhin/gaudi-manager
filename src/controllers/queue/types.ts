import { Document } from "mongoose";
import { Possibly } from "../../types.global";
export type QueueItem = {
    id: string;
    created: Date;
    tasks: string[];
};

export interface IQueueController {
    push(id: string, jobs: string[]): Promise<void>;
    pop(): Promise<Possibly<QueueItem>>;
    update(id: string, tasks: string[]): Promise<void>;
}

export type TourStatus = {
    id: string;
    status: "done" | "error";
};

export interface IQueueItem extends Document {
    id: string;
    created: Date;
    tasks: string[];
}
