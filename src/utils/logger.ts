export function devLog(message: string): void {
    if (process.env.NODE_ENV === "development") {
        console.log("\n[dev_log]: ", message, "\n");
    }
}
