import { gaudiApi } from "../api/gaudi";
import { JobStatusResponse, Statuses } from "controllers/job/types";

export async function getJobStatus(job: {
    id: string;
    update: string[];
}): Promise<{
    id: string;
    status: { tour?: Statuses; reconstruction?: Statuses };
}> {
    try {
        const updatedJob: {
            id: string;
            status: { tour?: Statuses; reconstruction?: Statuses };
        } = { id: job.id, status: {} };

        for await (const type of job.update) {
            switch (type) {
                case "tour":
                    const tourStatusRequest = await gaudiApi.get(
                        `/tour/jobs/${job.id}/`
                    );
                    const tourStatusResponse: JobStatusResponse =
                        tourStatusRequest.data;
                    const tourStatus =
                        tourStatusResponse.data.attributes.status;

                    updatedJob.status.tour = tourStatus;
                    break;

                case "reconstruction":
                    const reconstructionStatusRequest = await gaudiApi.get(
                        `/reconstruction/jobs/${job.id}/`
                    );

                    const reconstructionStatusResponse: JobStatusResponse =
                        reconstructionStatusRequest.data;

                    const reconstructionStatus =
                        reconstructionStatusResponse.data.attributes.status;

                    updatedJob.status.reconstruction = reconstructionStatus;
                    break;
            }
        }

        return updatedJob;
    } catch (error) {
        console.error(error);
        throw new Error(error);
    }
}
