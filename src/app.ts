import * as express from "express";
import * as bodyParser from "body-parser";
import * as mongoose from "mongoose";
import { Routes } from "./routes/job.routes";
import { dbConfig } from "./config/db.config";
// eslint-disable-next-line @typescript-eslint/no-var-requires
require("dotenv").config();

class App {
    public app: express.Application;
    public jobRoutes: Routes = new Routes();
    public dbUrl: string = dbConfig.url;
    public dbOptions: { [key: string]: boolean } = dbConfig.options;

    constructor() {
        this.app = express();
        this.config();
        this.jobRoutes.routes(this.app);
        this.dbSetup();
    }

    private dbSetup(): void {
        mongoose.connect(this.dbUrl, this.dbOptions);
    }

    private config(): void {
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: false }));
    }
}

export default new App().app;
