import axios from "axios";
import { AxiosInstance } from "axios";

let GAUDI_ORIGIN;
process.env.NODE_ENV === "development"
    ? (GAUDI_ORIGIN = process.env.GAUDI_MOCK)
    : (GAUDI_ORIGIN = process.env.GAUDI_ORIGIN);

export const gaudiApi: AxiosInstance = axios.create({
    baseURL: GAUDI_ORIGIN,
});
