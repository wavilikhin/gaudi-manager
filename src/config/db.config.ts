export const dbConfig = {
    url: `mongodb://mongo/managerserver`,
    options: {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
        useFindAndModify: false,
    },
};
