import * as fs from "fs";
import { join } from "path";
const multer = require("multer");
import { Request } from "express";
import { Multer } from "multer";

const storage = multer.diskStorage({
    destination: (
        req: Request,
        _file: Express.Multer.File,
        cb: (error: Error | null, dir: string) => void
    ) => {
        const { id } = req.params;

        if (!fs.existsSync(join(__dirname, "..", `tmp`))) {
            fs.mkdirSync(join(__dirname, "..", `tmp`));
        }

        if (!fs.existsSync(join(__dirname, "..", `tmp/${id}`))) {
            fs.mkdirSync(join(__dirname, "..", `tmp/${id}`));
        }

        const dir = join(__dirname, "..", `tmp/${id}`);

        return cb(null, dir);
    },
    filename: (
        _req: Request,
        file: Express.Multer.File,
        cb: (error: Error | null, filename: string) => void
    ) => {
        return cb(null, file.originalname.trim());
    },
});

export const upload: Multer = multer({ storage });
